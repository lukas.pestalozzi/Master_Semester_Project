\contentsline {section}{Abstract}{i}{Doc-Start}
\contentsline {section}{Contents}{ii}{section*.1}
\contentsline {section}{List of figures}{iv}{section*.2}
\contentsline {section}{List of tables}{iv}{section*.3}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}The game Tichu}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Rules}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Tactics}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Tichu and Artificial Intelligence}{4}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Difficulties}{5}{subsection.2.3.1}
\contentsline {paragraph}{Hidden Information}{5}{subsection.2.3.1}
\contentsline {paragraph}{Big branching factor}{5}{subsection.2.3.1}
\contentsline {paragraph}{Multiagent, cooperative and competitive}{5}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Implementation}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Architecture}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Difficulties}{6}{section.3.2}
\contentsline {chapter}{\numberline {4}Agents}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Background}{8}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Minimax Search}{8}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Monte Carlo Tree Search (MCTS)}{9}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Determinization and Perfect Information MCTS (PIMCTS)}{9}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Information Set MCTS (ISMCTS)}{10}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Simple and Random Agent}{10}{section.4.2}
\contentsline {section}{\numberline {4.3}Minimax Agent}{10}{section.4.3}
\contentsline {section}{\numberline {4.4}Monte Carlo Tree Search Agents}{11}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Default ISMCTS Agent}{11}{subsection.4.4.1}
\contentsline {subsubsection}{Tree selection}{11}{subsection.4.4.1}
\contentsline {subsubsection}{Evaluation}{12}{subsection.4.4.1}
\contentsline {subsubsection}{Selecting the best action at the end}{13}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Managing the branching factor}{13}{subsection.4.4.2}
\contentsline {subsubsection}{Episodic Information Capture and Reuse (EPIC)}{13}{subsection.4.4.2}
\contentsline {subsubsection}{Move groups}{14}{figure.caption.6}
\contentsline {subsection}{\numberline {4.4.3}Determinization}{14}{subsection.4.4.3}
\contentsline {subsubsection}{Random}{15}{subsection.4.4.3}
\contentsline {subsubsection}{The Data}{15}{subsection.4.4.3}
\contentsline {subsubsection}{Combination Determinization}{16}{figure.caption.8}
\contentsline {subsubsection}{Single Determinization}{17}{figure.caption.8}
\contentsline {subsubsection}{Machine Learning approach}{17}{figure.caption.8}
\contentsline {subsection}{\numberline {4.4.4}Rollout}{17}{subsection.4.4.4}
\contentsline {subsubsection}{Random Rollout}{17}{subsection.4.4.4}
\contentsline {subsubsection}{Last Good Response With Forgetting (LGRF)}{18}{subsection.4.4.4}
\contentsline {subsubsection}{No Rollout}{18}{subsection.4.4.4}
\contentsline {subsubsection}{Neural network Rollout}{18}{subsection.4.4.4}
\contentsline {section}{\numberline {4.5}Neural Network Agents}{19}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Architectures}{19}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Training}{20}{subsection.4.5.2}
\contentsline {chapter}{\numberline {5}Experiments}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Minimax, Random, default MCTS Tournament}{25}{section.5.1}
\contentsline {section}{\numberline {5.2}Cheating Tournament}{26}{section.5.2}
\contentsline {section}{\numberline {5.3}Reward Tournament}{27}{section.5.3}
\contentsline {section}{\numberline {5.4}Determinization Tournament}{28}{section.5.4}
\contentsline {section}{\numberline {5.5}EPIC Tournament}{28}{section.5.5}
\contentsline {section}{\numberline {5.6}Deep-Q learning Tournament}{29}{section.5.6}
\contentsline {section}{\numberline {5.7}Rollout Tournament}{30}{section.5.7}
\contentsline {section}{\numberline {5.8}Split Tournament}{31}{section.5.8}
\contentsline {section}{\numberline {5.9}Best Action Tournament}{32}{section.5.9}
\contentsline {section}{\numberline {5.10}Move Groups Match}{33}{section.5.10}
\contentsline {section}{\numberline {5.11}Playing against a Human}{34}{section.5.11}
\contentsline {chapter}{\numberline {6}Summary}{35}{chapter.6}
\contentsline {section}{\numberline {6.1}Future work}{35}{section.6.1}
\contentsline {section}{\numberline {6.2}Lessons Learned}{35}{section.6.2}
\contentsline {chapter}{Acknowledgments}{36}{chapter*.21}
\contentsline {chapter}{Author's Declaration}{37}{chapter*.22}
\contentsline {chapter}{Appendix}{38}{chapter*.23}
\contentsline {chapter}{Bibliography}{42}{figure.caption.30}
